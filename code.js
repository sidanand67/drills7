const products = [
    {
        shampoo: {
            price: "$50",
            quantity: 4,
        },
        "Hair-oil": {
            price: "$40",
            quantity: 2,
            sealed: true,
        },
        comb: {
            price: "$12",
            quantity: 1,
        },
        utensils: [
            {
                spoons: { quantity: 2, price: "$8" },
            },
            {
                glasses: { quantity: 1, price: "$70", type: "fragile" },
            },
            {
                cooker: { quantity: 4, price: "$900" },
            },
        ],
        watch: {
            price: "$800",
            quantity: 1,
            type: "fragile",
        },
    },
];


function productsPriceMoreThan65(){
    return products.reduce((acc,obj) => {
        Object.keys(obj).map(product => {
            if (Array.isArray(obj[product])){
                obj[product].map(prodObj => {
                    Object.keys(prodObj).map(prodObjKey => {
                        if (Number(prodObj[prodObjKey].price.slice(1)) > 65){
                            acc.push(prodObjKey);
                        }
                    })
                })
            }
            else {
                if (Number(obj[product].price.slice(1)) > 65){
                    acc.push(product); 
                }
            }
        }); 
        return acc; 
    }, []); 
}

function productsWithQtyMoreThan1() {
    return products.reduce((acc, obj) => {
        Object.keys(obj).map((product) => {
            if (Array.isArray(obj[product])) {
                obj[product].map(prodObj => {
                    Object.keys(prodObj).map(prodObjKey => {
                        if(prodObj[prodObjKey].quantity > 1){
                            acc.push(prodObjKey); 
                        }
                    }); 
                })
            }
            else {
                if(obj[product].quantity > 1){
                    acc.push(product); 
                }
            }
        });
        return acc;
    }, []);
}

function fragileItems() {
    return products.reduce((acc, obj) => {
        Object.keys(obj).map((product) => {
            if (Array.isArray(obj[product])) {
                obj[product].map((prodObj) => {
                    Object.keys(prodObj).map((prodObjKey) => {
                        if (prodObj[prodObjKey].type !== undefined) {
                            acc.push(prodObjKey);
                        }
                    });
                });
            } else {
                if (obj[product].type !== undefined) {
                    acc.push(product);
                }
            }
        });
        return acc;
    }, []);
}

function minMaxQtyPrice(){
    let min = 1000, max = 0; 
    return products.reduce((acc, obj) => {
        Object.keys(obj).map((product) => {
            if (Array.isArray(obj[product])) {
                obj[product].map((prodObj) => {
                    Object.keys(prodObj).map((prodObjKey) => {
                        if (Number(prodObj[prodObjKey].price.slice(1)) < min) {
                            min = Number(prodObj[prodObjKey].price.slice(1));
                            if (acc['minItem']) {
                                acc['minItem'] = prodObjKey; 
                            }
                            else {
                                acc['minItem'] = prodObjKey; 
                            }   
                        }
                        else if (Number(prodObj[prodObjKey].price.slice(1)) > max) {
                            max = Number(prodObj[prodObjKey].price.slice(1));
                            if (acc["maxItem"]) {
                                acc["maxItem"] = prodObjKey;
                            } else {
                                acc["maxItem"] = prodObjKey;
                            }   
                        }
                    });
                });
            } 
            else {
                if (Number(obj[product].price.slice(1)) < min) {
                    min = Number(obj[product].price.slice(1));
                    if (acc["minItem"]) {
                        acc["minItem"] = product;
                    } else {
                        acc["minItem"] = product;
                    }
                } else if (Number(obj[product].price.slice(1)) > max) {
                    max = Number(obj[product].price.slice(1));
                    if (acc["maxItem"]) {
                        acc["maxItem"] = product;
                    } else {
                        acc["maxItem"] = product;
                    }
                }
            }   
        });
        return acc;
    }, {});
}

function groupItemsByState(){
    return products.reduce((acc, obj) => {
        Object.keys(obj).map((product) => {
            if (Array.isArray(obj[product])) {
                obj[product].map((prodObj) => {
                    Object.keys(prodObj).map((prodObjKey) => {
                        if (prodObjKey.toLowerCase() === 'shampoo' || prodObjKey.toLowerCase() === 'hair-oil') {
                            if(acc.liquids){
                                acc.liquids.push(prodObjKey); 
                            }
                            else {
                                acc.liquids = [prodObjKey]; 
                            }
                        }
                        else {
                            if(acc.solids){
                                acc.solids.push(prodObjKey); 
                            }
                            else {
                                acc.solids = [prodObjKey]; 
                            }
                        }
                    });
                });
            } else {
                if (product.toLowerCase() === 'shampoo' || product.toLowerCase() === 'hair-oil') {
                    if (acc.liquids) {
                        acc.liquids.push(product);
                    } else {
                        acc.liquids = [product];
                    }
                }
                else {
                    if (acc.solids) {
                        acc.solids.push(product);
                    } else {
                        acc.solids = [product];
                    }
                }
            }
        });
        return acc;
    }, []);
}

// Function Calls 
let productsPriceMoreThan65List = productsPriceMoreThan65(); 
console.log(productsPriceMoreThan65List);  

let productsWithQtyMoreThan1List = productsWithQtyMoreThan1(); 
console.log(productsWithQtyMoreThan1List);

let fragileItemsList = fragileItems(); 
console.log(fragileItemsList); 

let minMaxQtyPriceList = minMaxQtyPrice();
console.log(minMaxQtyPriceList); 

let groupItems = groupItemsByState(); 
console.log(groupItems); 